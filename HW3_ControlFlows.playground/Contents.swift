//Title: FizzBuzz Count:
import UIKit

/*
    Write a program that prints the numbers from 1 to 100.
    But for multiples of three print "Fizz" instead of the number and for the multiples of five print "Buzz".
    For numbers which are multiples of both three and five print "FizzBuzz".
    Extra credit:  add a fourth print statement: "prime". You should print this whenever you encounter a number that is prime
*/


for counter in 1...100 {

    if ( (counter % 5) == 0 && (counter % 3) == 0)  {
        print("FizzBuzz");
    }
    else if ( (counter % 5 ) == 0 ) {
        print("Buzz");
    }
    else if ( (counter % 3) == 0) {
        print("Fizz");
    } else {
    // set isPrime equal to true if counter is not one
        var isPrime = counter == 1 ? false : true;
        
        // if counter is 2 then it is a prime and go straight to the print line.
        if (counter > 2) {
            for i in 2...counter {
                if (counter % i == 0) { // if the result is 0 then it can be divided which is not a prime number
                    isPrime = false; // is not a prime number because it can be divided
                }
            }
        }
        
        isPrime ? print("prime") : print(counter); // if isPrime is true print prime else print counter
        
    }
}


